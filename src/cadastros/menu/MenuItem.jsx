import React from "react";

const MenuItem = () => {

    return (
        <ul className={'navbar-nav'}>
            <li className={'nav-item'}>
                <a className={'nav-link'} aria-current={'page'} href={'/pais'}>Pais</a>
            </li>
            <li className={'nav-item'}>
                <a className={'nav-link'} aria-current={'page'} href={'#'}>Estado</a>
            </li>
            <li className={'nav-item'}>
                <a className={'nav-link'} aria-current={'page'} href={'#'}>Cidade</a>
            </li>
            <li className={'nav-item'}>
                <a className={'nav-link'} aria-current={'page'} href={'#'}>Bairro</a>
            </li>
            <li className={'nav-item'}>
                <a className={'nav-link'} href="#">Sair</a>
            </li>
        </ul>
    )
}

export default MenuItem;
