import React from "react";
import MenuItem from "./MenuItem";

const Header = () => {

    return (
        <nav className={'navbar navbar-expand-lg navbar-light bg-light'}>
            <div className={'container-fluid'}>
                <a className={'navbar-brand'} href={'#'}>Ruby on Rails 7.0</a>
                <div className={'collapse navbar-collapse'}>
                    <MenuItem/>
                </div>
            </div>
        </nav>
    )
}

export default Header;
