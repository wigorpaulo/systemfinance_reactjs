import Listagem from "./cadastros/pais/Listagem";
import Header from "./cadastros/menu/Header";
import Rota from "./rotas/Rota";

function App() {
  return (
    <div>
        <Header />
        <Rota />
    </div>
  );
}

export default App;
