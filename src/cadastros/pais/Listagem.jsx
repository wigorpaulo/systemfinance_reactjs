import React from "react";

const Listagem = () => {

    const lista_pais = [{id: 1, descricao: 'Brasil', sigla: 'BR'},
        {id: 2, descricao: 'Argentina', sigla: 'AG'},
        {id: 3, descricao: 'Alemanha', sigla: 'AL'}
    ];

    return (
        <div className={'container-xxl my-md-4 bd-layout'}>
            <h1>Listagem de Pais</h1>
            <br/>
            <a className={'btn btn-primary'} href={'/pais/new'}>Novo</a>
            <br/>

            <table className={'table'}>
                <thead>
                <tr>
                    <th scope={'col'}>#</th>
                    <th scope={'col'}>Descrição</th>
                    <th scope={'col'}>Sigla</th>
                    <th scope={'col'}>Ações</th>
                </tr>
                </thead>
                <tbody>
                {lista_pais.map((element) => (
                    <tr>
                        <th scope={'row'}>{element['id']}</th>
                        <td>{element['descricao']}</td>
                        <td>{element['sigla']}</td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    )
}

export default Listagem;
