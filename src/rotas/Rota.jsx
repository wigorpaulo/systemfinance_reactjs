import React from "react";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Listagem from "../cadastros/pais/Listagem";
import Form from "../cadastros/pais/Form";

const Rota = () => {

    return(
        <div>
            <Router>
                <Routes>
                    <Route path={'/pais'} element={<Listagem />} />
                    <Route path={'/pais/new'} element={<Form />} />
                </Routes>
            </Router>
        </div>
    )
}

export default Rota;
