import React, {useState} from "react";
import {useNavigate} from "react-router-dom";

const Form = () => {

    const navigate = useNavigate();
    const [dados, setDados] = useState({descricao: '', sigla: ''});

    const changeInputDescricao = (event) => {
        setDados({descricao: event.target.value});
    }

    const changeInputSigla = (event) => {
        setDados({sigla: event.target.value});
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        alert('Gravado com sucesso!');
        navigate('/pais');
    }

    return (
        <div className={'container-xxl my-md-4 bd-layout'}>
            <h1>Novo de Pais</h1>
            <br/>
            <a className={'btn btn-primary'} href={'/pais'}>Voltar</a>
            <br/>
            <br/>

            <form onSubmit={handleSubmit}>
                <div className={'mb-3'}>
                    <label className={'form-label'}>Descrição</label>
                    <input type={'text'} className={'form-control'} onChange={changeInputDescricao}/>
                </div>

                <div className={'mb-3'}>
                    <label className={'form-label'}>Sigla</label>
                    <input type={'text'} className={'form-control'} onChange={changeInputSigla}/>
                </div>

                <button className={'btn btn-success'} type={'submit'}>Gravar</button>
            </form>
        </div>
    )
}

export default Form;
